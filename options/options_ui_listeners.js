// onLoad listener to load Options and LegacyPrefs
document.addEventListener('DOMContentLoaded', async () => {
  consoleDebug("AHT: DOMContentLoaded, restoreAllOptions() and initLegacyPrefs()");
  restoreAllOptions();
  initLegacyPrefs();

  await messenger.messageContentPolicy.onChanged.addListener(async (newValue) => {
    consoleDebug("AHT: messageContentPolicy newValue:", newValue);
    await initLegacyPrefs();
  });

  document.addEventListener("visibilitychange", async () => {
    consoleDebug("AHT: DOMContent beforeunload");
    await messenger.messageContentPolicy.onChanged.removeListener(async () => {
      await initLegacyPrefs();
    });
  });
});

// onChange listener for all options to save the changed options
OptionsList.forEach((option) => {
  consoleDebug("AHT: OptionsList.forEach(): option: " + option);
  switch(option) {
    case "buttonHtmlMode":
      document.getElementById("buttonMode_html").addEventListener("change", (e) => {
        consoleDebug("AHT: OptionsList.forEach():  Option buttonHtmlMode changed, saveOptions()");
        saveOptions(e);
      });
      document.getElementById("buttonMode_sanitized").addEventListener("change", (e) => {
        consoleDebug("AHT: OptionsList.forEach():  Option buttonHtmlMode changed, saveOptions()");
        saveOptions(e);
      });
      document.getElementById("buttonMode_plaintext").addEventListener("change", (e) => {
        consoleDebug("AHT: OptionsList.forEach():  Option buttonHtmlMode changed, saveOptions()");
        saveOptions(e);
      });
      break;

    case "commandKey":
      document.getElementById("commandKey").addEventListener("change", async (e) => {
        consoleDebug("AHT: OptionsList.forEach():  Option commandKey changed, saveOptions()");
        if(testCommandKey(document.getElementById("commandKey").value) == true) {
          showCommandKeyWarning(false);
          saveOptions(e);
        } else {
          showCommandKeyWarning(true);
          saveOptions(e);
        }
      });
      break;

    default:
      document.getElementById(option).addEventListener("change", (e) => {
        consoleDebug("AHT: OptionsList.forEach():  Option " + option + " changed, saveOptions()");
        saveOptions(e);
        // .then(() => { enableOrDisableOptionUiElements(); });
      });
  }
});

// onChange listener for all options to save the changed prefs
PrefsList.forEach((option) => {
  consoleDebug("AHT: pref: " + option);
  switch(option) {
    case "appHtmlMode":
      document.getElementById("appMode_html").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option appHtmlMode (original HTML) changed, setPrefsMsgBodyAllowHTML()");
        setPrefsMsgBodyAllowHTML();
        prefChangedWarning("warning_appHtmlMode");
      });
      document.getElementById("appMode_sanitized").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option appHtmlMode (sanitized HTML) changed, setPrefsMsgBodySanitized()");
        setPrefsMsgBodySanitized();
        prefChangedWarning("warning_appHtmlMode");
      });
      document.getElementById("appMode_plaintext").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option appHtmlMode (plaintext) changed, setPrefsMsgBodyAsPlaintext()");
        setPrefsMsgBodyAsPlaintext();
        prefChangedWarning("warning_appHtmlMode");
      });
      document.getElementById("appMode_allBodyParts").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option appHtmlMode (AllBodyParts) changed, setPrefsMsgBodyAllParts()");
        setPrefsMsgBodyAllParts();
        prefChangedWarning("warning_appHtmlMode");
      });
      break;

    case "appRemoteContent":
      document.getElementById("appRemoteContent").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option appRemoteContent changed, setPrefsAppRemoteContent()");
        setPrefsAppRemoteContent();
        prefChangedWarning("warning_appRemoteContent");
      });
      break;

    case "allwaysInline":
      document.getElementById("allwaysInline").addEventListener("change", (e) => {
        consoleDebug("AHT: PrefsList.forEach():  Option allwaysInline changed, setPrefsAppAttachmentsInline()");
        setPrefsAppAttachmentsInline();
        prefChangedWarning("warning_allwaysInline");
      });
      break;
  }
});

// resetToDefault click listener
document.getElementById("resetToDefault").addEventListener("click", () => {
  consoleDebug("AHT: Reset clicked, resetToDefault()");
  resetToDefault();
  showCommandKeyWarning(false);
});

// resetToRecommended click listener
document.getElementById("resetToRecommended").addEventListener("click", () => {
  consoleDebug("AHT: Reset clicked, resetToRecommended()");
  resetToRecommended();
  prefChangedWarning("warning_appHtmlMode");
  prefChangedWarning("warning_appRemoteContent");
  prefChangedWarning("warning_allwaysInline");
  showCommandKeyWarning(false);
});

function prefChangedWarning (elementId) {
  let element = document.getElementById(elementId);
  if (element.classList.contains("hidden")) {
    element.classList.remove("hidden");
    consoleDebug("AHT: options_ui_listeners.js: prefChangedWarning: element.classList.remove");
  }
}

function testCommandKey(testValue) {
  consoleDebug("AHT: testCommandKey: " + testValue);

  let detail = {};
  detail.name = "_execute_message_display_action";
  detail.shortcut = testValue;

  if(detail.shortcut === "") {
    return false;
  }

  try {
    messenger.commands.update(detail)
    return true;
  } catch (e) {
    return false;
  }
}

function showCommandKeyWarning(doWarn) {
  if(doWarn) {
    let element = document.getElementById("warning_optionCommandKey");
    if (element.classList.contains("hidden")) {
      element.classList.remove("hidden");
      consoleDebug("AHT: options_ui_listeners.js: warning_optionCommandKey: element.classList.remove");
    }
  } else {
    let element = document.getElementById("warning_optionCommandKey");
    if (!element.classList.contains("hidden")) {
      element.classList.add("hidden");
      consoleDebug("AHT: options_ui_listeners.js: warning_optionCommandKey: element.classList.add");
    }
  }
}
